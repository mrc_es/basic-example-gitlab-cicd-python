from main import return_false
from main import return_true

def test_return_true():
    assert return_true() == True

def test_return_false():
    assert return_false() == False

